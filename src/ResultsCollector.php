<?php

namespace hosttechPackages\StatusCheck;

class ResultsCollector
{
    private $allChecksPassed = true;
    private $results;

    /**
     * Result constructor.
     */
    public function __construct()
    {
        $this->results = collect();
    }

    /**
     * @return bool
     */
    public function allChecksPassed(): bool
    {
        return $this->allChecksPassed;
    }

    /**
     * Add a new result object to the collection
     *
     * @param Result $result
     */
    public function addResult(Result $result) : void
    {
        $this->results->add($result);

        $this->allChecksPassed = $this->allChecksPassed && $result->isPassed();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getResults(): \Illuminate\Support\Collection
    {
        return $this->results;
    }
}
