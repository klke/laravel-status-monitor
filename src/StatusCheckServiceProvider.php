<?php

namespace hosttechPackages\StatusCheck;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class StatusCheckServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'status-check');

        $this->registerRoutes();
        $this->bootSchedulerCheck();

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../stubs/StatusCheckServiceProvider.stub' => app_path('Providers/StatusCheckServiceProvider.php'),
            ], 'provider');

            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('status-check.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'status-check');
    }

    /**
     * Register routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        $url = config('status-check.url', 'status');

        Route::get($url, StatusCheckController::class)
            ->middleware('web')
            ->name('status-check.index');
    }

    /**
     * Registers a scheduled command that will trigger on every run.
     */
    private function bootSchedulerCheck()
    {
        $checks = config('status-check.checks', []);

        // Only register the scheduled command if the check is active
        if (!in_array(\hosttechPackages\StatusCheck\Checks\Scheduler::class, $checks)) {
            return;
        }

        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);

            $schedule->call(function () {
                // Update a cache value with the current time stamp.
                // The value will be cached for 5 minutes, after which an error will be reported
                Cache::set('hosttechPackages\StatusCheck\Checks\Scheduler', now()->timestamp, now()->addMinutes(5));
            })
                ->everyMinute()
                ->runInBackground();
        });
    }
}
