<?php

namespace hosttechPackages\StatusCheck\Checks;

use hosttechPackages\StatusCheck\Result;

/**
 * Class Queues
 *
 * @package hosttechPackages\StatusCheck\Checks
 */
class Queues extends \hosttechPackages\StatusCheck\Checks\Check
{
    /**
     * @return Result
     */
    public function run(): Result
    {
        $environment = app()->environment();
        $this->runChecks(config('horizon.environments.' . $environment, []));

        return $this->result;
    }

    /**
     * @param array $environment
     */
    protected function runChecks(array $environment): void
    {
        foreach (array_keys($environment) as $queue) {
            $this->runCheck($queue);
        }
    }

    /**
     * @param string $queue
     */
    protected function runCheck(string $queue): void
    {
        $maxPending = data_get($this->config, 'max-pending.' . $queue, 0);
        $size = \Queue::size($queue);

        $passed = $maxPending >= $size;

        $this->result->updatePassed($passed);
        $this->result->appendData([
            'state'       => $passed,
            'name'        => $queue,
            'size'        => $size,
            'max-pending' => $maxPending,
        ]);
    }
}
