<?php

namespace hosttechPackages\StatusCheck\Checks;

use BeyondCode\SelfDiagnosis\Checks\Check;
use hosttechPackages\StatusCheck\Result;

/**
 * Class SelfDiagnosis
 *
 * @package hosttechPackages\StatusCheck\Checks
 */
class SelfDiagnosis extends \hosttechPackages\StatusCheck\Checks\Check
{
    /**
     * @return Result
     */
    public function run() : Result
    {
        $checks = data_get($this->config, 'checks', config('self-diagnosis.checks', []));
        $exclude = data_get($this->config, 'exclude', []);

        $checks = collect($checks)->filter(function ($config, $check) use ($exclude) {
            if (is_numeric($check)) {
                $check = $config;
            }

            return !in_array($check, $exclude);
        })->all();

        $this->runChecks($checks);

        $environment = app()->environment();
        $environmentChecks = config('self-diagnosis.environment_checks.' . $environment, []);

        if (empty($environmentChecks) && array_key_exists($environment, config('self-diagnosis.environment_aliases'))) {
            $environment = config('self-diagnosis.environment_aliases.' . $environment);
            $environmentChecks = config('self-diagnosis.environment_checks.' . $environment, []);
        }

        $this->runChecks($environmentChecks);

        return $this->result;
    }

    /**
     * @param array $checks
     */
    protected function runChecks(array $checks): void
    {
        foreach ($checks as $check => $config) {
            if (is_numeric($check)) {
                $check = $config;
                $config = [];
            }

            $checkClass = app($check);
            $this->runCheck($checkClass, $config);
        }
    }

    /**
     * @param Check $check
     * @param array $config
     */
    protected function runCheck(Check $check, array $config): void
    {
        $passed = $check->check($config);

        $this->result->updatePassed($passed);
        $this->result->appendData([
            'state'   => $passed,
            'name'    => $check->name($config),
            'message' => $passed ?: $check->message($config),
        ]);
    }
}
