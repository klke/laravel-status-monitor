<?php

namespace hosttechPackages\StatusCheck\Checks;

use Carbon\Carbon;
use hosttechPackages\StatusCheck\Result;
use Illuminate\Support\Facades\Cache;

/**
 * Class Scheduler
 *
 * @package hosttechPackages\StatusCheck\Scheduler
 */
class Scheduler extends \hosttechPackages\StatusCheck\Checks\Check
{
    /**
     * @return Result
     */
    public function run(): Result
    {
        $lastRun = Cache::get('hosttechPackages\StatusCheck\Checks\Scheduler', false);

        $this->result->setPassed($lastRun);

        $this->result->appendData([
            'last-run' => !!$lastRun ? Carbon::createFromTimestamp($lastRun) : null,
        ]);

        return $this->result;
    }
}
