<?php

namespace hosttechPackages\StatusCheck\Checks;

use hosttechPackages\StatusCheck\Result;
use Illuminate\Support\Str;

/**
 * Class Check
 *
 * @package hosttechPackages\StatusCheck\Checks
 */
abstract class Check
{
    /**
     * @var \hosttechPackages\StatusCheck\Result
     */
    protected $result;

    /**
     * @var array
     */
    protected $config;

    /**
     * Check constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->result = app(Result::class);
        $this->result->view = self::getViewName();
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    abstract public function run(): Result;

    /**
     * Returns the name for the view that should be used to render this check
     */
    public static function getViewName(): string
    {
        $className = substr(static::class, strrpos(static::class, '/') + 1);

        return Str::kebab($className);
    }
}
