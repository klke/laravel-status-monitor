<?php

namespace hosttechPackages\StatusCheck;

/**
 * Class Result
 *
 * @package hosttechPackages\StatusCheck
 */
class Result
{
    public $view;

    private $passed = true;

    private $additionalData;

    /**
     * Result constructor.
     */
    public function __construct()
    {
        $this->additionalData = collect();
    }


    /**
     * @return bool
     */
    public function isPassed(): bool
    {
        return $this->passed;
    }

    /**
     * Sets the status of passed to the defined value, regardless of the previous state.
     *
     * @param bool $passed
     */
    public function setPassed(bool $passed): void
    {
        $this->passed = $passed;
    }

    /**
     * Updates the status of passed with the defined values. Once the status has failed, it will remain false.
     *
     * @param bool $passed
     */
    public function updatePassed(bool $passed): void
    {
        $this->passed = $this->passed && $passed;
    }

    /**
     * Append additional data to the result object
     *
     * @param $payload
     */
    public function appendData($payload)
    {
        $this->additionalData->add($payload);
    }

    /**
     * @return \Illuminate\Support\Collection | mixed
     */
    public function getAdditionalDataArray()
    {
        return $this->additionalData->toArray();
    }

    /**
     * @param string $key
     *
     * @return \Illuminate\Support\Collection | mixed
     */
    public function getAdditionalData(string $key = null)
    {
        $firstItem = $this->additionalData->first();

        if ($key && $firstItem) {
            return $firstItem[$key];
        }

        return $this->additionalData;
    }
}
