<?php

namespace hosttechPackages\StatusCheck;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class StatusCheckApplicationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->authorization();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Configure the StatusCheck authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();

        StatusCheck::auth(function ($request) {
            return app()->environment('local') ||
                Gate::check('viewStatusCheck', [$request->user()]);
        });
    }

    /**
     * Register the StatusCheck gate.
     *
     * This gate determines who can access StatusCheck in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewStatusCheck', function ($user) {
            return in_array($user->email, [
            ]);
        });
    }
}
