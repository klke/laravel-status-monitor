<?php

namespace hosttechPackages\StatusCheck;

use Closure;
use hosttechPackages\StatusCheck\Checks\Check;

class StatusCheck
{
    /**
     * The callback that should be used to authenticate StatusCheck users.
     *
     * @var \Closure
     */
    private static $authUsing;

    /**
     * Determine if the given request can access the Horizon dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public static function check($request)
    {
        return (static::$authUsing ?: function () {
            return app()->environment('local');
        })($request);
    }

    /**
     * Set the callback that should be used to authenticate StatusCheck users.
     *
     * @param  \Closure  $callback
     * @return static
     */
    public static function auth(Closure $callback)
    {
        static::$authUsing = $callback;

        return new static();
    }

    /**
     * Runs all status checks and sets the results object
     */
    public static function run(): ResultsCollector
    {
        $checks = config('status-check.checks', []);

        $resultsCollector = app(ResultsCollector::class);

        collect($checks)
            ->map(function ($config, $class) {
                if (is_numeric($class)) {
                    $class = $config;
                    $config = [];
                }

                return new $class($config);
            })
            ->each(function (Check $check) use ($resultsCollector) {
                $resultsCollector->addResult($check->run());
            });

        return $resultsCollector;
    }
}
