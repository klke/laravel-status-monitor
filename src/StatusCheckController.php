<?php

namespace hosttechPackages\StatusCheck;

use App;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class StatusCheckController
 *
 * @package hosttechPackages\StatusCheck
 */
class StatusCheckController extends BaseController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        App::setLocale('en');
        $resultsCollection = StatusCheck::run();
        $showDetails = StatusCheck::check(request());
        $tests = $resultsCollection->getResults()->first()->getAdditionalDataArray();
        $statusCode = $resultsCollection->allChecksPassed() ? 200 : 412;

        $results = [
            'passed' =>  $resultsCollection->allChecksPassed(),
            'tests' => $tests,
            'app_env' => app()->environment(),
            'load_time' => round(microtime(true) - LARAVEL_START, 3),
        ];

        return response()->json($results, $statusCode);
        /*
        return response()
            ->view('status-check::index', compact('resultsCollection', 'showDetails'), $statusCode);
        */
    }
}
