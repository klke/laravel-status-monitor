# Status Check

This package registers a web route, commonly `/status`, and performs various 
checks to ensure the application is healthy and correctly working. The status
page can then be included in a monitoring solution, to ensure you are alerted
as soon as an error arises.

![Screenshot](screenshot.png)

## Installation

Add the following repository to your composer.json:
```json
{
  "repositories": [
    { "type": "git", "url": "https://git.hosttech.eu/packages/laravel-status-check.git" }
  ]
}
```

Now you can install the package via composer:
```sh
composer require hosttech-packages/laravel-status-check
```

Then execute the publish command to create the config and service provider file:
```sh
php artisan vendor:publish --provider="hosttechPackages\StatusCheck\StatusCheckServiceProvider"
```

And register the new service provider:
```php
// config/app.php
return [
    'providers' => [
        // ...
        App\Providers\StatusCheckServiceProvider::class
    ]
];
```

## Usage

laravel-status-check registers the `/status` URL. When navigating to this URL, 
the package will run all the configured tests and display the application 
state.

By default, you will only be able to see the detailed report in the `local` 
environment. Within your `app/Providers/StatusCheckServiceProvider.php` file,
there is a `gate` method. This authorization gate controls whether StatusCheck 
shows detailed information in **non-local** environments. You are free to 
modify this gate as needed to restrict access to your StatusCheck installation:

```php

/**
 * Register the StatusCheck gate.
 *
 * This gate determines who can access StatusCheck in non-local environments.
 *
 * @return void
 */
protected function gate()
{
    Gate::define('viewStatusCheck', function ($user) {
        return in_array($user->username, [
            'admin',
        ]);
    });
}
```


## Available Checks

The Package comes preconfigured with 3 checks, but you can customize the checks performed, as well as write your own.
To change parameters for checks, review the configuration settings in `/config/status-check.php`.

### Queues

This check ensures the queue is running and that it is not getting overworked. If the queue workers are to slow, 
the pending jobs might start to pile up. If there are more pending jobs than configured, the check will fail.

### Scheduler

This Check will register a scheduled task to run each minute and update a cache value with the current timestamp. 
When performing the status check, the cached timestamp will be compared with the current time. If the difference is 
more than 5 minutes the check will fail, indicating that the scheduler has not run in the last 5 minutes.

Note that this implementation depends on the laravel cache. If you are running the laravel application on multiple 
servers, with shared caches, for example, this check might not work.

### SelfDiagnosis

This check performs various checks on the overall application health. To do this, it depends on the
[laravel-self-diagnosis](https://github.com/beyondcode/laravel-self-diagnosis) package. It can check that the correct 
PHP version is running on the server and all required extensions are enabled. Further, it validates that laravel 
specific settings like cache drivers are configured and horizon is started via supervisor.

The package provides a separate configuration file for the checks it performs. Therefore you should make sure that the 
configuration matches your application requirements.


## Creating custom Checks

You can create a custom check to perform some application specific check. 
To do this, create a class that extends `\hosttechPackages\StatusCheck\Checks\Check`.
The class must implement a `run` method that returns `\hosttechPackages\StatusCheck\Result`.

```php
<?php

namespace App\Utilities\StatusCheck\Checks;

use hosttechPackages\StatusCheck\Result;

class CustomCheck extends \hosttechPackages\StatusCheck\Checks\Check
{
    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        // perform check
        $passed = $this->performSomeCheck();

        $this->result->updatePassed($passed);

        return $this->result;
    }
}
```

To display the status for this custom check on the status page, 
create a view in `resources/views/vendor/status-check/checks` with 
the kebab-case version of the class name for your check. So for the
example above, this file would be called `custom-check.blade.php`.

```html
<div class="card mt-5">
    <div class="card-header">Custom Check</div>
    <div class="card-body">
        @if($result->isPassed())
            <span>
                ✔ Check was successful.
            </span>
        @else
            <span class="text-danger">
                ❌ There is something wrong.
            </span>
        @endif
    </div>
</div>
```
