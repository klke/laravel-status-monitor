# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.2.1] - 2020-05-08

### Changed

- Updated package dependencies, no breaking changes.


## [1.2.0] - 2020-03-27

Added compatibility with Laravel 7.


## [1.1.0] - 2020-03-25

Added a config option to the `SelfDiagnosis` check to allow some checks to be excluded.


## [1.0.0] - 2020-02-12

Initial Release