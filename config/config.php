<?php

return [

    /*
     * The url that will be registered when you call  StatusCheck::routes();
     */
    'url'    => 'monitor/health',

    /*
     * The checks that will be executed in order
     */
    'checks' => [
        /*
         * Runs the checks from beyondcode/laravel-self-diagnosis
         */
        \hosttechPackages\StatusCheck\Checks\SelfDiagnosis::class => [
            'exclude' => [],
        ],

        /*
         * Checks that the last scheduler is not older than one minute.
         */
        //\hosttechPackages\StatusCheck\Checks\Scheduler::class,

        /*
         * Checks the status of a queue and how many jobs are in the backlog
         */
        /*
        \hosttechPackages\StatusCheck\Checks\Queues::class => [
            'max-pending' => [
                'default' => 500,
            ],
        ],
        */
    ],
];
