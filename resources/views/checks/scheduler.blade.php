<div class="card mt-5">
    <div class="card-header">Scheduler</div>
    <div class="card-body">
        @if($result->isPassed())
            <span>
                ✔ Scheduler ran last {{ $result->getAdditionalData('last-run')->diffForHumans() }}.
            </span>
        @else
            <span class="text-danger">
                ❌ Scheduler did not run in the last 5 minutes.
            </span>
        @endif
    </div>
</div>