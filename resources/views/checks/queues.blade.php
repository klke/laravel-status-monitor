<div class="card mt-5">
    <div class="card-header">Queue</div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">Queue</th>
            <th scope="col">Pending Jobs</th>
            <th scope="col">Max Pending</th>
        </tr>
        </thead>
        <tbody>
        @foreach($result->getAdditionalData() as $check)
            <tr class="{{ $check['state'] ?: 'table-danger' }}">
                <th scope="row" width="30">{{ $check['state'] ? '✔' : '❌'}}</th>
                <td>{{ $check['name'] }}</td>
                <td>{{ $check['size'] }}</td>
                <td>{{ $check['max-pending'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>