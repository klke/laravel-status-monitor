<div class="card mt-5">
    <div class="card-header">SelfDiagnosis</div>
    <table class="table">
        <tbody>
        @foreach($result->getAdditionalData() as $check)
            <tr class="{{ $check['state'] ?: 'table-danger' }}">
                <th scope="row" width="30">{{ $check['state'] ? '✔' : '❌'}}</th>
                <td>
                    {{ $check['name'] }}
                    @unless($check['state'])
                        <br/><br/>{{ $check['message'] }}
                    @endunless
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>