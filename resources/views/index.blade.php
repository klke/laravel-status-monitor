@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if($resultsCollection->allChecksPassed())
                    <div id="system-status" class="system-status-ok card text-white bg-success">
                        <div class="card-header">All services operational.</div>
                    </div>
                @else
                    <div id="system-status" class="system-status-error card text-white bg-danger">
                        <div class="card-header">Some services are experiencing issues.</div>
                    </div>
                @endif

                @if($showDetails)
                    @foreach($resultsCollection->getResults() as $result)
                        @includeIf('status-check::checks.'.$result->view, ['result' => $result])
                    @endforeach
                @else
                    <div class="card mt-5">
                        <div class="card-body">
                            To view detailed information, please
                            <a class="alert-link" href="{{ route('login') }}">{{ __('Login') }}</a>.
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
